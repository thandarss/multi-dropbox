import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { NavigationContainer } from "@react-navigation/native";
import { StyleSheet } from "react-native";
import { useEffect, useState } from "react";
import * as Font from "expo-font";
//Temp
import { MultiDropBox } from "./src/components/basics/MultiDropBox";

const Stack = createNativeStackNavigator();
export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);
  const [user, setUser] = useState<any | null>(null);
  const [initializing, setInitializing] = useState(true);

  useEffect(() => {
    const loadFonts = async () => {
      await Font.loadAsync({
        SpaceMono: require("./src/assets/fonts/SpaceMono-Regular.ttf"),
        HiraginoKaku_GothicPro_Text: require("./src/assets/fonts/Hiragino-Kaku-Gothic-Pro-W3.otf"),
        HiraginoKaku_GothicPro_Text_Bold: require("./src/assets/fonts/Hiragino-Kaku-Gothic-Pro-W6.ttf"),
      });

      setFontLoaded(true);
    };
    loadFonts();
  }, []);

  if (!fontLoaded) {
    return null;
  }

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="MultiDropBox"
        screenOptions={{
          headerShown: false,
          animation: "none",
        }}
      >
        {/* Temp */}
        <Stack.Screen name="MultiDropBox" component={MultiDropBox}/>
        
      </Stack.Navigator>
    </NavigationContainer>
  );
}
